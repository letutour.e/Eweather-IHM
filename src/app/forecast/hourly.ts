import {HourlyData} from './hourlyData';

export class Hourly{
  public summary: string;
  public icon: string;
  public data: HourlyData[];
}
