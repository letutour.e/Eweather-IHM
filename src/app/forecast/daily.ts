import {DailyData} from './dailyData';

export class Daily{
  public summary: string;
  public icon: string;
  public data: DailyData[];
}
