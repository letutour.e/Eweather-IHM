import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EweatherIhmComponent } from './eweather-ihm/eweather-ihm.component';
import { CurrentlyComponent } from './eweather-ihm/currently/currently.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatExpansionModule} from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HourlyComponent } from './eweather-ihm/hourly/hourly.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { DailyComponent } from './eweather-ihm/daily/daily.component';
import { HelpComponent } from './help/help.component';

@NgModule({
  declarations: [
    AppComponent,
    EweatherIhmComponent,
    CurrentlyComponent,
    HourlyComponent,
    DailyComponent,
    HelpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatTabsModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    FontAwesomeModule,
    MatExpansionModule,
    LeafletModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
